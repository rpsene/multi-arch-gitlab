#!/bin/bash

export DOCKER_CLI_EXPERIMENTAL=enabled

docker manifest create quay.io/rpsene/techu2021:multi-arch-gitlab-latest \
quay.io/rpsene/techu2021:multi-arch-gitlab-x86_64 quay.io/rpsene/techu2021:multi-arch-gitlab-ppc64le

docker manifest inspect quay.io/rpsene/techu2021:multi-arch-gitlab-latest

docker login quay.io -u "$ROBOT_USER" -p $ROBOT_TOKEN

docker manifest push quay.io/rpsene/techu2021:multi-arch-gitlab-latest
